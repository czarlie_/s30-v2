// 2) Use the count operator to count the total number of fruits on sale
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: 'totalNumOfFruitOnSale' }])

// 3) Use the count operator to count the total number of fruits with stock more than or equal to 20
db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  { $count: 'Total number of fruits with stock ≥ 20' },
])

// 4) Use the average operator to get the average price of fruits onSale per supplier
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: 'fruitsOnSale', avgPrice: { $avg: '$price' } } },
])

// 5) Use the max operator to get the highest price of a fruit per supplier
db.fruits.aggregate([
  { $group: { _id: '$supplier_id', highestPrice: { $max: '$price' } } },
])

// 6) Use the min operator to get the lowest price of a fruit per supplier
db.fruits.aggregate([
  { $group: { _id: '$supplier_id', lowestPrice: { $min: '$price' } } },
])
