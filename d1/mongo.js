// We are going to create four documents to use for the discussion

db.fruits.insertMany([
  {
    name: 'Apple',
    color: 'Red',
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ['Philippines', 'US'],
  },

  {
    name: 'Banana',
    color: 'Yellow',
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ['Philippines', 'Ecuador'],
  },

  {
    name: 'Kiwi',
    color: 'Green',
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ['US', 'China'],
  },

  {
    name: 'Mango',
    color: 'Yellow',
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ['Philippines', 'India'],
  },
])

/*
* MongoDB Aggregation *
    What is an Aggregate?
      - a cluster of things that have come or have been brought together
      > In MongoDB, aggregation operations group values from multiple documents together <   
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', total: { $sum: '$stock' } } },
])

// Aggregation Pipeline Stages
// Aggregation is typically done in two to three steps. Each process in aggregation is called a STAGE
/*
 * $match *
    - is used to match or get documents which SATISFIES the condition
      SYNTAX:
        { $match: { field: <value> }}

 * $group *
    - allows us to group together documents and create an analysis out of the grouped documents

    _id: in the group stage, essentially associates an id to our results
    _id: also, it determines the number of groups
*/

// Field projection with aggregation
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', total: { $sum: '$stock' } } },
  { $project: { _id: 0 } },
])

/*
  * $project *
      - can be used when aggregating data to include or exclude fields from the returned results
        Syntax:
          { $project: { field: 1/0}}
          1 means show up
          0 means does not show up
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', total: { $sum: '$stock' } } },
  { $sort: { total: -1 } },
])

/*
  * $sort *
    - can be used to change the orderr of aggregated results
      Syntax:
        { $sort { field: 1/-1 }}
        1 means ascending
       -1 means descending
*/

// Aggregating results based on array fields:
db.fruits.aggregate([{ $unwind: '$origin' }])

/*
  * $unwind *
    - deconstructs an array field from a field with our array value to output a result for each element
      Syntax:
        { $unwind: field}
*/
db.fruits.aggregate([
  { $unwind: '$origin' },
  { $group: { _id: '$origin', kinds: { $sum: 1 } } },
])

// Mini Activity #1:
// Find how many fruits are yellow
db.fruits.aggregate([{ $match: { $color: 'Yellow' } }, { $count: 'yellow' }])

/*
  $count
    Syntax:
      {$count: <string>}
*/

// Mini Activity #2:
// Find how many fruit stocks that are less than or equal to 10
// db.fruits.find({ stocks: { $lte: 10 } })
db.fruits.aggregate([
  { $match: { stock: { $lte: 10 } } },
  { $count: 'lowOnStocks:' },
])

// Mini Activity #3:
// Find fruits for each group of supplier that has the maximum stocks
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', max_stock: { $max: '$stock' } } },
])
